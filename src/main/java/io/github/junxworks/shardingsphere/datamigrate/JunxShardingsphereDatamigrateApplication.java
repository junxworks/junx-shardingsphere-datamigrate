package io.github.junxworks.shardingsphere.datamigrate;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.github.junxworks.ep.core.EnableGlobalExceptionHandler;
import io.github.junxworks.ep.core.ds.EnableDynamicDataSource;

@EnableDynamicDataSource //开启EP数据源配置
@EnableGlobalExceptionHandler //开启全局异常捕获
@MapperScan(basePackages = { "io.github.junxworks.*" }, annotationClass = Mapper.class) //io.github.junxworks.ep是ep的mapper扫苗，还可以添加项目自身的mapper扫描，如com.xxx.ccc.mapper
@SpringBootApplication
public class JunxShardingsphereDatamigrateApplication {

	public static void main(String[] args) {
		SpringApplication.run(JunxShardingsphereDatamigrateApplication.class, args);
	}

}
