/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  Config.java   
 * @Package io.github.junxworks.ep.example.config   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021-1-24 15:35:52   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.shardingsphere.datamigrate.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.github.junxworks.junx.core.executor.StandardThreadExecutor;
import io.github.junxworks.junx.event.EventBus;
import io.github.junxworks.junx.event.EventChannel;
import io.github.junxworks.junx.event.impl.ExecutorEventChannel;
import io.github.junxworks.junx.event.impl.SimpleEventBus;
import io.github.junxworks.shardingsphere.datamigrate.core.Signal;
import io.github.junxworks.shardingsphere.datamigrate.core.Topics;
import io.github.junxworks.shardingsphere.datamigrate.core.handler.DataStoreHandler;

@Configuration
@EnableConfigurationProperties({ MigrateConfig.class })
public class Config {

	@Bean(initMethod = "start", destroyMethod = "stop")
	public EventBus eventBus() {
		SimpleEventBus eventBus = new SimpleEventBus();
		eventBus.setName("Global-Event-Bus");
		return eventBus;
	}

	@Bean
	public Signal signal() {
		return new Signal();
	}

	@Bean(name = "extractExecutor")
	public StandardThreadExecutor extractExecutor(MigrateConfig config) {
		StandardThreadExecutor executor = new StandardThreadExecutor();
		executor.setName("extract-executor");
		executor.setConfig(config.getExtractExecutor());
		return executor;
	}

	@Bean(name = "writeExecutor")
	public StandardThreadExecutor writeExecutor(MigrateConfig config) {
		StandardThreadExecutor executor = new StandardThreadExecutor();
		executor.setName("write-executor");
		executor.setConfig(config.getWirteExecutor());
		return executor;
	}

	@Bean
	public EventChannel dataStoreChannel(EventBus eventBus, DataStoreHandler handler, StandardThreadExecutor writeExecutor) {
		ExecutorEventChannel channel = new ExecutorEventChannel("DataStore-Channel", Topics.STORE_DATA);
		channel.setExecutor(writeExecutor);
		channel.setEventHandler(handler);
		eventBus.registerChannel(channel);
		return channel;
	}
}
