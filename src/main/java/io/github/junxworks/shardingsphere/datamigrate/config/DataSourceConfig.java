/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  DataSourceConfig.java   
 * @Package io.github.junxworks.ep.example.config   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021-1-24 15:35:52   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.shardingsphere.datamigrate.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;

import io.github.junxworks.ep.core.ds.DynamicDataSource;
import io.github.junxworks.ep.core.ds.DynamicDataSourceBuilder;
import io.github.junxworks.shardingsphere.datamigrate.Constants;

@Configuration
public class DataSourceConfig {

	@Bean
	@ConfigurationProperties("spring.datasource.druid.primary")
	public DataSource primaryDataSource() {
		return DruidDataSourceBuilder.create().build();
	}

	@Bean
	@ConfigurationProperties("spring.datasource.druid.target")
	public DataSource targetDataSource() {
		return DruidDataSourceBuilder.create().build();
	}

	@Bean
	@Primary
	public DynamicDataSource dynamicDataSource(DataSource primaryDataSource, DataSource targetDataSource) {
		return DynamicDataSourceBuilder.create().setPrimarySource("primary", primaryDataSource).addDataSource(Constants.DS_TARGET, targetDataSource).build();
	}
}
