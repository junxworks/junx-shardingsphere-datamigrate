/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  MigrateConfig.java   
 * @Package io.github.junxworks.shardingsphere.datamigrate.config   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021年9月26日 下午7:43:04   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.shardingsphere.datamigrate.config;

import java.util.Set;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.google.common.collect.Sets;

import io.github.junxworks.junx.core.executor.ExecutorConfig;

@ConfigurationProperties(prefix = "junx.sharding.migrate")
public class MigrateConfig {

	/** 写入数据的时候，是否需要清除主键值. */
	private boolean clearPrimaryKeyValue = true;

	/** 默认的表主键名. */
	private String defaultPrimaryKeyName = "id";

	/** 需要进行数据迁移的表名 */
	private Set<String> tables = Sets.newHashSet();

	/** 数据抽取每次分页查询数据量. */
	private int extractBatchSize = 2000;

	/** 数据每批次写入数据条数，不建议太大，要跟读取保持平衡，可以跟写入线程池配合使用，建议批量写入数据前，去掉表的索引 */
	private int writeBatchSize = 2000;

	/** 数据抽取线程池配置 */
	private ExecutorConfig extractExecutor = new ExecutorConfig();

	/** 数据写入线程池配置. */
	private ExecutorConfig wirteExecutor = new ExecutorConfig();

	public String getDefaultPrimaryKeyName() {
		return defaultPrimaryKeyName;
	}

	public void setDefaultPrimaryKeyName(String defaultPrimaryKeyName) {
		this.defaultPrimaryKeyName = defaultPrimaryKeyName;
	}

	public boolean isClearPrimaryKeyValue() {
		return clearPrimaryKeyValue;
	}

	public void setClearPrimaryKeyValue(boolean clearPrimaryKeyValue) {
		this.clearPrimaryKeyValue = clearPrimaryKeyValue;
	}

	public Set<String> getTables() {
		return tables;
	}

	public void setTables(Set<String> tables) {
		this.tables = tables;
	}

	public ExecutorConfig getExtractExecutor() {
		return extractExecutor;
	}

	public void setExtractExecutor(ExecutorConfig extractExecutor) {
		this.extractExecutor = extractExecutor;
	}

	public ExecutorConfig getWirteExecutor() {
		return wirteExecutor;
	}

	public void setWirteExecutor(ExecutorConfig wirteExecutor) {
		this.wirteExecutor = wirteExecutor;
	}

	public int getExtractBatchSize() {
		return extractBatchSize;
	}

	public void setExtractBatchSize(int extractBatchSize) {
		this.extractBatchSize = extractBatchSize;
	}

	public int getWriteBatchSize() {
		return writeBatchSize;
	}

	public void setWriteBatchSize(int writeBatchSize) {
		this.writeBatchSize = writeBatchSize;
	}

}
