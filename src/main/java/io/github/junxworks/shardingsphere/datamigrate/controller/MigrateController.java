package io.github.junxworks.shardingsphere.datamigrate.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.github.junxworks.ep.core.Result;
import io.github.junxworks.shardingsphere.datamigrate.core.Engine;

@RestController
@RequestMapping("/data-migrate")
public class MigrateController {

	@Autowired
	private Engine engine;

	@GetMapping("/start")
	public Result startExtractTask() {
		engine.start();
		return Result.ok();
	}

	@GetMapping("/shutdown")
	public Result stopExtractTask() {
		engine.stop();
		return Result.ok();
	}

	@GetMapping("/restart")
	public Result restartExtractTask() {
		engine.restart();
		return Result.ok();
	}

	@GetMapping("/suspend")
	public Result suspendExtractTask() {
		engine.suspend();
		return Result.ok();
	}

	@GetMapping("/resume")
	public Result resumeExtractTask() {
		engine.resume();
		return Result.ok();
	}
}
