package io.github.junxworks.shardingsphere.datamigrate.core;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.github.junxworks.ep.core.utils.JsonUtils;
import io.github.junxworks.junx.core.exception.FatalException;
import io.github.junxworks.junx.core.executor.StandardThreadExecutor;
import io.github.junxworks.junx.core.lifecycle.Service;
import io.github.junxworks.junx.core.util.StringUtils;
import io.github.junxworks.shardingsphere.datamigrate.config.MigrateConfig;
import io.github.junxworks.shardingsphere.datamigrate.core.extractor.DBDataExtractor;
import io.github.junxworks.shardingsphere.datamigrate.core.extractor.ExtractContext;

@Component
public class Engine extends Service {

	@Autowired
	private Signal signal;

	@Autowired
	@Qualifier("extractExecutor")
	private StandardThreadExecutor extractExecutor;

	@Autowired
	@Qualifier("writeExecutor")
	private StandardThreadExecutor writeExecutor;

	@Autowired
	private DBDataExtractor extractor;

	@Autowired
	private MigrateConfig config;

	@Override
	protected void doStart() throws Throwable {
		try {
			signal.start();
			extractExecutor.start();
			writeExecutor.start();
		} catch (Exception e) {
			throw new FatalException(e);
		}
		Set<String> tables = config.getTables();
		if (tables.isEmpty()) {
			logger.warn("没有用于数据抽的表");
			return;
		}
		logger.info("启动如下表数据抽取:" + JsonUtils.objectToString(tables));
		int batchSize = config.getExtractBatchSize();
		tables.forEach(t -> {
			extractExecutor.submit(new Runnable() {
				@Override
				public void run() {
					ExtractContext context = new ExtractContext();
					context.setTopic(Topics.STORE_DATA);
					String[] tableKey = StringUtils.replace(t, " ", "").split("\\|");
					if (tableKey.length == 0 || tableKey.length > 2) {
						logger.warn("用于数据抽的表配置异常:" + JsonUtils.objectToString(tableKey));
						return;
					}
					context.setTargetTable(tableKey[0]);
					context.setClearPrimaryKeyValue(config.isClearPrimaryKeyValue());
					if (tableKey.length == 1) {
						context.setPrimaryKey(config.getDefaultPrimaryKeyName());
					} else {
						context.setPrimaryKey(tableKey[1]);
					}
					context.setSql("select * from " + context.getTargetTable());
					context.setPageSize(batchSize);
					try {
						extractor.extract(context);
					} catch (Exception e) {
						logger.error(context.getTargetTable() + "表数据抽异常", e);
						return;
					}
				}
			});
		});
	}

	@Override
	protected void doStop() throws Throwable {
		try {
			signal.shutdown();
		} catch (Exception e) {
			throw new FatalException(e);
		}
		extractExecutor.stop();
		writeExecutor.stop();
	}

	protected void doSuspend() throws Throwable {
		signal.suspend();
	}

	protected void doResume() throws Throwable {
		signal.resume();
	}
}
