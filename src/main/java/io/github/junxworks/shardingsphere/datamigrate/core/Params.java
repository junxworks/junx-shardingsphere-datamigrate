package io.github.junxworks.shardingsphere.datamigrate.core;

public class Params {

	public static final String PARAM_EXTRACT_OBJS = "_extract_objs_";

	public static final String PARAM_TABLE_NAME = "_table_name_";

}
