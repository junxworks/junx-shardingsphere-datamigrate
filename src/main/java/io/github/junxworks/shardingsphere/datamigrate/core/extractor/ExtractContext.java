/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  ExtractContext.java   
 * @Package io.github.junxworks.shardingsphere.datamigrate.core.extractor   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021年9月25日 上午9:30:48   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.shardingsphere.datamigrate.core.extractor;

/**
 * 抽取上下文
 */
public class ExtractContext {

	/** 查询条件. */
	private String sql;

	/** 总查询结果大小.-1表示所有 */
	private int capacity = -1;

	/** 分页查询大小. */
	private int pageSize = 500;

	/** 发送事件名 */
	private String topic;

	/** 抽取目标表名 */
	private String targetTable = "_target_";

	/** 主键名 */
	private String primaryKey;

	/** 写入数据的时候，是否需要清除主键值. */
	private boolean clearPrimaryKeyValue = true;

	public boolean isClearPrimaryKeyValue() {
		return clearPrimaryKeyValue;
	}

	public void setClearPrimaryKeyValue(boolean clearPrimaryKeyValue) {
		this.clearPrimaryKeyValue = clearPrimaryKeyValue;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getTargetTable() {
		return targetTable;
	}

	public void setTargetTable(String targetTable) {
		this.targetTable = targetTable;
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		this.sql = sql;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
